#!/bin/bash
# get the current workspace
ws=$( xprop -root _NET_CURRENT_DESKTOP | sed -e 's/_NET_CURRENT_DESKTOP(CARDINAL) = //' )

# icons

CURRENT=∙
OCCUPIED=∙
UNOCCUPIED=⋅

Workspace[0]="web"
Workspace[1]="dev"
Workspace[2]="media"
Workspace[3]="anim"
Workspace[4]="misc"

. "${HOME}/.cache/wal/colors.sh"

# colors
fg="${#FFFFFF}"
bg="${color0:-#000000}"
fg1="${color10:-#000000}"
fg2="${color8:-#000000}"
fg3="${color4:-#000000}"

#  print workspaces to stdout
draw() {
    for i in {0..5}; do
        # get the number of windows in each workspace
        windows=$( wmctrl -l | cut -d ' ' -f3 | grep $i | wc -l )

        if [[ $i -eq $ws ]]
        then
            if [[ $windows -gt 0 ]]
            then
              echo -ne "%{B$fg1} %{F$fg} ${Workspace[i]}  "
            else
              echo -ne "%{B$fg1} %{F$fg} ${Workspace[i]}  "
            fi
            # current workspace
            #if [ "$i" -eq 0 ]
            #then
            #  echo -ne "%{B$fg1} %{F$fg} web  "
            #fi
            #if [ "$i" -eq 1 ]
            #then
            #  echo -ne "%{B$fg1} %{F$fg} dev  "
            #fi
            #if [ "$i" -eq 2 ]
            #then
            #  echo -ne "%{B$fg1} %{F$fg} media  "
            #fi
            #if [ "$i" -eq 3 ]
            #then
            #  echo -ne "%{B$fg1} %{F$fg} misc  "
            #fi
            #if [ "$i" -eq 4 ]
            #then
            #  echo -ne "%{B$fg1} %{F$fg} work  "
            #fi
        else
            #if [ "$i" -eq 0 ]
            #then
            if [[ $windows -gt 0 ]]
            then
              echo -ne "%{B$bg} %{F$fg} ${Workspace[i]}  "
            else
              echo -ne "%{B$bg} %{F$fg2} ${Workspace[i]}  "
            fi
            #fi
            #if [ "$i" -eq 1 ]
            #then
            #  echo -ne "%{B$bg} %{F$fg2} dev  "
            #fi
            #if [ "$i" -eq 2 ]
            #then
            ##  echo -ne "%{B$bg} %{F$fg2} media  "
            #fi
            #if [ "$i" -eq 3 ]
            #then
            #  echo -ne "%{B$bg} %{F$fg2} misc  "
            #fi
            #if [ "$i" -eq 4 ]
            #then
            #  echo -ne "%{B$bg} %{F$fg2} work  "
            #fi
        fi
    done
}

draw
