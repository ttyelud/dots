call plug#begin()
Plug 'roxma/nvim-completion-manager'
Plug 'SirVer/ultisnips'
Plug 'honza/vim-snippets'
Plug 'dylanaraps/wal.vim'
Plug 'scrooloose/nerdtree'
Plug 'yuttie/comfortable-motion.vim'
Plug 'tpope/vim-vinegar'
Plug 'terryma/vim-multiple-cursors'
Plug 'digitaltoad/vim-pug'
Plug 'Yggdroot/indentLine'
Plug 'kien/ctrlp.vim'
call plug#end()

syntax enable
set noswapfile
set number
set nowrap

hi LineNr ctermfg=grey ctermbg=white

colorscheme wal

" Minimal Settings
set laststatus=0
set noshowmode
set noshowcmd
set noruler

set clipboard=unnamed

hi CursorLine   cterm=NONE ctermbg=NONE ctermfg=magenta

hi VertSplit	cterm=NONE ctermbg=NONE ctermfg=NONE

let NERDTreeMinimalUI = 1
let NERDTreeDirArrows = 1


set mouse=a
set fillchars=""

autocmd ColorScheme * highlight VertSplit cterm=NONE cterm=NONE
autocmd bufenter * if (winnr("$") == 1 && exists("b:NERDTree") && b:NERDTree.isTabTree()) | q | endif
autocmd vimenter * NERDTree
autocmd VimEnter * wincmd p

let g:NERDTreeChDirMode = 2
hi LineNr ctermfg=235 

map <C-o> :NERDTreeToggle %<CR>


set tabstop=2 shiftwidth=2 expandtab
let g:indentLine_leadingSpaceEnabled = 1
let g:indentLine_leadingSpaceChar = '·'
